<?php
namespace lib;

abstract class BaseForm 
{
        
    protected $errors = [];
    protected $data = [];
        
    public function __get($name) {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return NULL;
    }

    /**
     * load data from $_POST or $_GET
     * 
     * @param array $formData
     * @return boolean
     */
    public function load(array $formData)
    {
        if (empty($formData)) {
            return false;
        }
        $listAttributes = array_keys($this->attributes());
        $this->clearErrors();
        $this->clearData();
        foreach ($listAttributes as $attr) {
            if (isset($formData[$attr])) {
                $this->data[$attr] = $this->getDataFromOut($attr, $formData[$attr]);
            }
        }
        return true;
    }
    
    /**
     * Returns a value indicating whether there is any validation error.
     * @return boolean
     */
    public function hasError()
    {
        return !empty($this->errors);
    }
    
    /**
     * Returns the errors for all attributes
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    
    /**
     * Performs the data validation
     * 
     * @return boolean
     */
    public function validate()
    {
      
        $this->clearErrors();
        foreach ($this->attributes() as $attrName => $attr) {
            if (isset($attr['required']) && $attr['required'] && $this->$attrName === NULL) {
                $this->addError($attrName, "No set required param '$attrName'");
            }
            if ($this->$attrName !== NULL && in_array($attr['type'], ['integer', 'number']) &&
                    isset($attr['min']) && $attr['min'] > $this->$attrName) {
                $this->addError($attrName, "MIN value for '$attrName' is {$attr['min']}");
            }
            if ($this->$attrName !== NULL && in_array($attr['type'], ['integer', 'number']) &&
                    isset($attr['max']) && $attr['max'] < $this->$attrName) {
                $this->addError($attrName, "MAX value for '$attrName' is {$attr['max']}");
            }
            if ($this->$attrName !== NULL && $attr['type'] === 'range' &&
                    isset($attr['in']) && is_array($attr['in']) && !in_array($this->$attrName, $attr['in'])) {
                $this->addError($attrName, $attrName . ' value not in range');
            }
        }
        return !$this->hasError();
    }
    
    public function getData()
    {
        return $this->data;
    }
    
    abstract public function attributes();
    
    protected  function getDataFromOut($attrName, $setvalue)
    {
        if (isset($this->attributes()[$attrName]) && isset($this->attributes()[$attrName]['type'])) {
            return filter_var($setvalue, 
                     (isset($this->sanitaze()[$this->attributes()[$attrName]['type']]) ? 
                    $this->sanitaze()[$this->attributes()[$attrName]['type']] : FILTER_SANITIZE_STRING));
        }
        return filter_var($setvalue, FILTER_SANITIZE_STRING);
    }   
    
    protected  function addError($attrName, $errorMessage)
    {
        $this->errors[$attrName] = $errorMessage;
    }
    
    /**
     * clear errors history
     */
    protected function clearErrors()
    {
        $this->errors = [];
    }
    
    /**
     * empty data
     */
    protected function clearData()
    {
        $this->data = [];
    }
    
    protected function sanitaze() {
        return [
            'integer' => FILTER_VALIDATE_INT,
            'number' => FILTER_VALIDATE_FLOAT,
            'string' => FILTER_SANITIZE_STRING
        ];
    }
}

