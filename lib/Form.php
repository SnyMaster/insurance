<?php
namespace lib;

use lib\DataInterface;

class Form extends BaseForm implements DataInterface
{
           
    public function getEstimate() {
        return $this->estimate;   
    }
    
    public function getInstalments() {
        return $this->instalments;
    }
    
    public function getTax() {
        return $this->tax;
    }
    
    public function getUserDay() {
        return $this->userDay;
    }
    
    public function getUserHour() {
        return $this->userHour;
    }
    
    /**
     * return list attributes with valdation rules
     * 
     * @return array
     */                
    public function attributes()
    {
        return [
            'estimate' => ['type' => 'integer', 'min' => 100, 'max' => 100000, 'required' => TRUE],
            'tax' => ['type' => 'integer', 'min' => 1, 'max' => 100, 'required' => TRUE],
            'instalments' => ['type' => 'integer', 'min' => 1, 'max' => 12, 'required' => TRUE],
            'userDay' => ['type' => 'range', 'in' => ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], 'required' => TRUE],
            'userHour' => ['type' => 'integer', 'min' => 0, 'max' => 24, 'required' => TRUE],
        ];
    }    
}

