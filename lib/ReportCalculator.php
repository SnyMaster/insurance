<?php
namespace lib;

use lib\ReportInterface;

class ReportCalculator implements ReportInterface
{
    /**
     * Return view
     * 
     * @param array $data
     * @return string
     */
    public function renderReport(array $data) {
        $report = '<table class="report-calculator"><thead><tr><th class="first-column">&nbsp;</th><th>Policy</th>';
        for ($i = 1; $i <= count($data['price']); $i++) {
            $report .= "<th> $i Instalment </th>";
        }
        $report .= '</th></tr></thead><tbody><tr><th>Value, EUR</th><td  class="sum">' . number_format($data['estimate'], 2) . '</td>';
        for ($i = 1; $i <= count($data['price']); $i++) {
            $report .= "<td>&nbsp;</td>";
        }
        $report .= '</tr><tr><th>Base premium (' . $data['pricePercent'] . '%), EUR</th><td class="sum">' . number_format(array_sum($data['price']), 2) . '</td>';
        foreach ($data['price'] as $val) {
            $report .= '<td class="sum">' . number_format($val, 2) . '</td>';
        }
        $report .= '</tr><tr><th>Comission (' . $data['commitionPercent'] . '%), EUR</th><td class="sum">' . number_format(array_sum($data['commition']), 2) . '</td>';
        foreach ($data['commition'] as $val) {
            $report .= '<td class="sum">' . number_format($val, 2) . '</td>';
        }
        $report .= '</tr><tr><th>Tax (' . $data['taxPercent'] . '%), EUR</th><td class="sum">' . number_format(array_sum($data['tax']), 2) . '</td>';
        foreach ($data['tax'] as $val) {
            $report .= '<td class="sum">' . number_format($val, 2) . '</td>';
        }
        $report .= '</tr><tr><th><strong>Total cost, EUR</strong></th><td class="sum"><strong>' . 
                number_format(($data['estimate'] + array_sum($data['price']) + array_sum($data['commition']) + array_sum($data['tax'])), 2) . 
                '</strong></td>';
        for ($i = 0; $i < count($data['price']); $i++) {
            $total = $data['price'][$i] + $data['commition'][$i] + $data['tax'][$i];
            $report .= '<td class="sum">' .number_format($total, 2) . '</td>';
        }
        $report .= '</tr></tbody></table>';
        return  $report;
    }
}

