<?php
namespace lib;
use lib\BuilderCalculator;

class Calculator 
{
    private $estimate;
    private $tax;
    private $commition;
    private $instalments;
    private $basePrice;
    private $userDay;
    private $userHour;
    private $extendPrice;
    private $extendPriceHourFrom;
    private $extendPriceHourTo;
    private $extendPriceDay;
    
    public function __construct(BuilderCalculator $builder)
    {    
        if (!isset($builder->estimate)) {
            throw new \InvalidArgumentException('Empty "estimate" for Calculator');
        }
        $this->estimate = $builder->estimate;
        
        if (!isset($builder->tax)) {
            throw new \InvalidArgumentException('Empty "tax" for Calculator');
        }        
        $this->tax = $builder->tax;
        
        if (!isset($builder->commition)) {
            throw new \InvalidArgumentException('Empty "commition" for Calculator');
        }
        $this->commition = $builder->commition;
        
        if (!isset($builder->instalments)) {
            throw new \InvalidArgumentException('Empty "instalments" for Calculator');
        }
        $this->instalments = $builder->instalments;
        
        if (!isset($builder->basePrice)) {
            throw new \InvalidArgumentException('Empty "basePrice" for Calculator');
        }
        $this->basePrice = $builder->basePrice;
        
        if (!isset($builder->userDay)) {
            throw new \InvalidArgumentException('Empty "userDay" for Calculator');
        }
        $this->userDay = $builder->userDay;
        
        if (!isset($builder->userHour)) {
            throw new \InvalidArgumentException('Empty "userHour" for Calculator');
        }
        $this->userHour = $builder->userHour; 
        
        if (!isset($builder->extendPrice)) {
            throw new \InvalidArgumentException('Empty "extendPrice" for Calculator');
        }
        $this->extendPrice = $builder->extendPrice;
        
        if (!isset($builder->extendPriceDay)) {
            throw new \InvalidArgumentException('Empty "extendPriceDay" for Calculator');
        }
        $this->extendPriceDay = $builder->extendPriceDay;
        
        if (!isset($builder->extendPriceHourFrom)) {
            throw new \InvalidArgumentException('Empty "extendPriceHourFrom" for Calculator');
        }
        $this->extendPriceHourFrom = $builder->extendPriceHourFrom;
        
        if (!isset($builder->extendPriceHourTo)) {
            throw new \InvalidArgumentException('Empty "extendPriceHourTo" for Calculator');
        }
        $this->extendPriceHourTo = $builder->extendPriceHourTo;
    }
    
    public function calculate(){
        $price = $this->calculatePrice();
        return [
            'estimate' => $this->estimate,
            'price' => $price,
            'commition' => $this->calculateCommition(array_sum($price)),
            'tax' => $this->calculateTax(array_sum($price)),
            'pricePercent' => $this->getCalcPrice(),
            'commitionPercent' => $this->commition,
            'taxPercent' => $this->tax,
        ];
    }
    
    public function calculatePrice()
    {
        $calcPrice = $this->getCalcPrice();
        return $this->splitSum($this->estimate * ($calcPrice / 100));
    }
    
    public function calculateCommition($price)
    {
        return $this->splitSum(round($price * ($this->commition / 100), 2));
    }
    
    public function calculateTax($price)
    {
        return $this->splitSum(round($price * ($this->tax/ 100)));
    }
    
    public function getCalcPrice()
    {
        return ($this->userDay === $this->extendPriceDay && 
                $this->userHour >= $this->extendPriceHourFrom && 
                $this->userHour <= $this->extendPriceHourTo) ?
                $this->extendPrice : $this->basePrice;
    }
    
    private function splitSum($sum)
    {
        $sumPart = $sum / $this->instalments;
        if ($sum === round($sumPart, 2) * $this->instalments) {
            return array_fill(0, $this->instalments, round($sumPart, 2));
        }
        $countDown = intval(explode('.', number_format(round($sumPart, 2) * $this->instalments - $sum, 2))[1]);
        $countUp = $this->instalments - $countDown;
        $arrUpDown = round($sumPart, 2) * $this->instalments - $sum < 0 ? 
                array_fill($countUp, $countDown, ceil($sumPart * 100) / 100) : 
                array_fill($countUp, $countDown, floor($sumPart * 100) / 100);
        return array_fill(0, $countUp, round($sumPart, 2)) + $arrUpDown;
    }
}
