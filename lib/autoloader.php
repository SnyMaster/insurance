<?php
spl_autoload_register(function ($class)
{
    $baseDir = dirname(dirname(__FILE__));
    $filename = $baseDir . '/' . str_replace('\\', '/', $class) . '.php';
    include($filename);
});