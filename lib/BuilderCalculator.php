<?php
namespace lib;

use lib\Calculator;
use \InvalidArgumentException;
use \RangeException;

class BuilderCalculator
{
    public $estimate;
    public $tax;
    public $commition;
    public $instalments;
    public $basePrice;
    public $userDay;
    public $userHour;
    public $extendPrice = null;
    public $extendPriceHourFrom = null;
    public $extendPriceHourTo = null;
    public $extendPriceDay = null;
    
    const WEEK_DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    
    public function setEstimate($estimate)
    {
        $this->estimate = (int)$estimate;
        return $this;
    }
    
    /**
     * Set Tax percentage
     * 
     * @param float $value
     * @return $this
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setTax($value)
    {
        if (!is_numeric($value)) {
            throw new InvalidArgumentException('Argument "tax" must be numeric value.');
        }
        if ($value < 0) {
            throw new RangeException('Argument "tax" could not be less 0.'); 
        }
        if ($value > 100) {
            throw new RangeException('Argument "tax" could not be more 100.');
        }
        $this->tax = floatval($value);
        return $this;
    } 
    
    /**
     * Set percent commition (add to tax)
     * 
     * @param type $value
     * @return $this
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setCommition($value)
    {
        if (!is_numeric($value)) {
            throw new InvalidArgumentException('Argument "commition" must be numeric value.');
        }
        if ($value < 0) {
            throw new RangeException('Argument "commition" could not be less 0.'); 
        }
        $this->commition = floatval($value);
        return $this;
    }   

    /**
     * Set installments. value range: 1 - 12
     * 
     * @param type $value
     * @return $this
     * 
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setInstalments($value)
    {
        if (!is_integer($value)) {
            throw new InvalidArgumentException('Argument "installments" must be integer value.');
        }
        if ($value < 1) {
            throw new RangeException('Argument "installments" could not be less 1.'); 
        }
        if ($value > 12) {
            throw new RangeException('Argument "installments" could not be more 12.');
        }
        $this->instalments = intval($value);
        return $this;
    }
    
    /**
     * Set percent base price
     * 
     * @param type $value
     * @return $this
     * 
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setBasePrice($value)
    {
        if (!is_numeric($value)) {
            throw new InvalidArgumentException('Argument "base price" must be numeric value.');
        }
        if ($value < 0) {
            throw new RangeException('Argument "base price" could not be less 0.'); 
        }
        if ($value > 100) {
            throw new RangeException('Argument "base price" could not be more 100.');
        }
        $this->basePrice = floatval($value);
        return $this;
    }
    
    /**
     * Set current user day
     * 
     * @param string $userDay
     * @return $this
     * 
     * @throws InvalidArgumentExceptionn
     */
    public function setUserDay($userDay)
    {
        if (!in_array($userDay, self::WEEK_DAYS)) {
            throw new InvalidArgumentException('Argument "user day" incorrect'); 
        }
        $this->userDay = $userDay;
        return $this;
    }
    
    /**
     * Set current user hour
     * 
     * @param integer $value
     * 
     * @return $this
     * 
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setUserHour($value)
    {
        if (!is_integer($value)) {
            throw new InvalidArgumentException('Argument "user hour" must be integer value.');
        }
        if ($value < 0) {
            throw new RangeException('Argument "user hour" could not be less 0.'); 
        }
        if ($value > 23) {
            throw new RangeException('Argument "user hour" could not be more 23.');
        }
        $this->userHour = (int) $value;
        return $this;
    }
    
    /**
     * Set value for extend price
     * 
     * @param type $value
     * @return $this
     * 
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setExtendPrice($value)
    {
        if (!is_numeric($value)) {
            throw new InvalidArgumentException('Argument "extend price" must be numeric value.');
        }
        if ($value < 0) {
            throw new RangeException('Argument "extend price" could not be less 0.'); 
        }
        if ($value > 100) {
            throw new RangeException('Argument "extend price" could not be more 100.');
        }
        $this->extendPrice = floatval($value);
        return $this;
    }
    
    /**
     * Set start hour for period when used extend price
     * 
     * @param integer $value
     * @return $this
     * 
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setExtendPriceHourFrom($value)
    {
        if (!is_integer($value)) {
            throw new InvalidArgumentException('Argument "hour from" must be integer value.');
        }
        if ($value < 0) {
            throw new RangeException('Argument "hour from" could not be less 0.'); 
        }
        if ($value > 23) {
            throw new RangeException('Argument "hour from" could not be more 23.');
        }
        if (!empty($this->extendPriceHourTo) && $this->extendPriceHourTo <= intval($value)) {
            throw new InvalidArgumentException('Argument "hour to" could not be less or equal "hour from".');
        }
        $this->extendPriceHourFrom = (int) $value;
        return $this;
    }
    
    /**
     * Set end hour for period when used extend price
     * 
     * @param integer $value
     * @return $this
     * 
     * @throws InvalidArgumentException
     * @throws RangeException
     */
    public function setExtendPriceHourTo($value)
    {
        if (!is_integer($value)) {
            throw new InvalidArgumentException('Argument "hour to" must be integer value.');
        }
        if ($value < 0) {
            throw new RangeException('Argument "hour to" could not be less 0.'); 
        }
        if ($value > 23) {
            throw new RangeException('Argument "hour to" could not be more 23.');
        }
        if (!empty($this->extendPriceHourFrom) && $this->extendPriceHourFrom >= intval($value)) {
            throw new InvalidArgumentException('Argument "hour to" could not be more or equal "hour from".');
        }
        $this->extendPriceHourTo = intval($value);
        return $this;
    }
    
    /**
     * Set day when used extend price
     * 
     * @param string $value
     * 
     * @return $this
     * @throws InvalidArgumentExceptionn
     */
    public function setExtendPriceDay($value)
    {
        if (!in_array($value, self::WEEK_DAYS)) {
            throw new InvalidArgumentException('Argument "extend price day" incorrect'); 
        }
        $this->extendPriceDay = $value;
        return $this;
    }

    /**
     * Return Calculator
     * 
     * @return Calculator
     */
    public function getCalculator()
    {
        return new Calculator($this);
    }

}
