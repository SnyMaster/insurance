<?php

namespace lib;

interface ReportInterface
{
    public function renderReport(array $data);
}
