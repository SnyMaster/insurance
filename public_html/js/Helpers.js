var Form = function (formId, submitHundler){
    this._formId = formId;
    this._forma = document.getElementById(this._formId);
    this._inputFields = this._forma.getElementsByTagName("input");  
    this._submitHundler = submitHundler;
    Array.from(this._inputFields).forEach((elemInput) => { 
            elemInput.addEventListener("blur", () => { this.checkField(elemInput);});
            elemInput.errors =[];
        });
    Array.from(this._inputFields).forEach((elemInput) => { 
            elemInput.addEventListener("input", () => { this.checkField(elemInput);});
            elemInput.errors =[];
        });
    this._forma.addEventListener("submit", () => {
        event.preventDefault();
        if (this.validate()) {
            let formData = new FormData(this._forma);
            this._submitHundler(this._forma.getAttribute("action"), formData);
        }
    });
    
    this.validate = function() {
        let listErrors = [];
        const ev = new Event("blur");
        Array.from(this._inputFields).forEach( (elemInput) => {
            elemInput.dispatchEvent(ev);
            listErrors = listErrors.concat(elemInput.errors);
        });
        return !Boolean(listErrors.length);
    };

    this.clearForm = function() {
        this._forma.reset();
        Array.from(this._inputFields).forEach( (el) => el.classList.remove("error"));
        const errElem = document.getElementsByClassName("error-message");
        Array.from(errElem).forEach((elm) => { elm.innerHTML = ""; });                        
        this._inputFields[0].focus();
    };

    this.checkRules = {
        number : function(elem) {
            //elem.errors = [];
            if (!elem.value.match(/^[\-]?[0-9]+$/)) {
                elem.errors.push("Value must be a number");
            }
            if (elem.errors.length === 0 && elem.dataset.rangeMin !== undefined && 
                    Number.parseInt(elem.dataset.rangeMin) > Number.parseInt(elem.value)) {
                elem.errors.push("MIN value " + elem.dataset.rangeMin);   
            }
            if (elem.errors.length === 0 && elem.dataset.rangeMax !== undefined && 
                   Number.parseInt(elem.dataset.rangeMax) < Number.parseInt(elem.value)) {
               elem.errors.push("MAX value " + elem.dataset.rangeMax);   
            } 
            return;
        },
        max : function(elem, rangeValue) {
            if (!elem.value.match(/^[\-]?[0-9]+$/)) {
                elem.errors.push("Value must be a number with max value " + rangeValue);
            }
            rangeValue = Number.parseInt(rangeValue);
            if ( !isNaN(rangeValue) && rangeValue < Number.parseFloat(elem.value)) {
                elem.errors.push("MAX value " + rangeValue);   
            }
            return;
        },
        min : function(elem, rangeValue) {
            if (!elem.value.match(/^[\-]?[0-9]+$/)) {
                elem.errors.push("Value must be a number with max value " + rangeValue);
            }
            rangeValue = Number.parseInt(rangeValue);
            if ( !isNaN(rangeValue) && rangeValue > Number.parseFloat(elem.value)) {
                elem.errors.push("MIN value " + rangeValue);   
            }
            return;
        },
        required : function(elem) {
            if (elem.value.trim().length === 0) {
                elem.errors.push("Please enter value");
            }
            return;
        },
        lenmin : function(elem, lenValue) {
            lenValue = Number.parseInt(lenValue);
            if (!isNaN(lenValue) && lenValue > elem.value.trim().length) {
                elem.errors.push("Min string length is " + lenValue);
            }
            return;
        },
        lenmax : function(elem, lenValue) {
            lenValue = Number.parseInt(lenValue);
            if (!isNaN(lenValue) && lenValue < elem.value.trim().length) {
                elem.errors.push("Max string length is " + lenValue);
            }
            return;
        }
        
    };

    this.checkField = function(elem) {            
        elem.errors = [];
        if (elem.dataset.rules !== undefined) {
            const rules = elem.dataset.rules.split("|");
            rules.forEach( (rule) => { 
                let ruleParam;
                [rule, ruleParam] = rule.split(":");
                if (elem.errors.length === 0 && this.checkRules[rule] !== undefined) { 
                    this.checkRules[rule](elem, ruleParam);
                } 
            });
        } else {
            return;
        }
        if (elem.errors.length === 0) {
            elem.classList.remove("error");
            document.getElementById("error-" + elem.getAttribute("name")).innerHTML = "";
        } else {
        elem.classList.add("error");                            
        document.getElementById("error-" + elem.getAttribute("name")).innerHTML = elem.errors.join(", ");    
        }
    };    
    
    this.setSubmitHundler = function(submitHundler) {
        this._submitHundler = submitHundler;
    };
    
    this.setInputValue = function(nameInput, valueInput) {
        Array.from(this._inputFields).forEach( (elemInput) => { 
            if (elemInput.name === nameInput) {
                elemInput.value = valueInput;
            }
        });
    };
    
};

var localStorageHelper = function() {
    this.isLocalStorageAvailable = function() {
        let storage;
        try {
            storage = window["localStorage"];
            const x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        }
        catch(e) {
            return e instanceof DOMException && (
                // everything except Firefox
                e.code === 22 ||
                // Firefox
                e.code === 1014 ||
                // test name field too, because code might not be present
                // everything except Firefox
                e.name === 'QuotaExceededError' ||
                // Firefox
                e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
                // acknowledge QuotaExceededError only if there's something already stored
                (storage && storage.length !== 0);
        }
    };         
    
    if (!this.isLocalStorageAvailable()) {
        this._storage = null;
    } else {
        this._storage = window.localStorage;
    }
    
    this.storageNotAvailabel = function() {
        alert("The local storage is not avalable in this browser.");
    }
    this.save = function(keyName, keyValue) {
        if (!this._storage) {
            this.storageNotAvailabel();
            return;
        }
        this._storage.setItem(keyName, keyValue);
    };
    
    this.get = function(keyName) {
        if (!this._storage) {
            this.storageNotAvailabel();
            return;
        }
        return this._storage.getItem(keyName);
    };

};