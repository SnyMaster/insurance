<?php

use lib\Form;
use lib\BuilderCalculator;
use lib\ReportCalculator;

include('../lib/autoloader.php');

if (!empty($_POST)) {
    $formData = new Form();
    try {   
        $formData->load($_POST);
        if ($formData->validate()) {

                $calc = (new BuilderCalculator())->setEstimate($formData->getEstimate())
                        ->setTax($formData->getTax())
                        ->setInstalments($formData->getInstalments())
                        ->setBasePrice(11)
                        ->setUserDay($formData->getUserDay())
                        ->setUserHour($formData->getUserHour())
                        ->setCommition(17)
                        ->setExtendPrice(20)
                        ->setExtendPriceDay('Fri')
                        ->setExtendPriceHourFrom(15)
                        ->setExtendPriceHourTo(20)
                        ->getCalculator();
                echo (new ReportCalculator())->renderReport($calc->calculate());
        } else {
            echo '<p class="error-message">' . implode('<br/>', $formData->getErrors()) . '</p>';
        }
    } catch (\Exception $ex) {
        echo '<p class="error-message">' . $ex->getMessage() . '</p>';
    }    
}