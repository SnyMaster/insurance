<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use lib\Form;

class FormTest extends TestCase
{
    public function testCreateForm()
    {
        $form = new Form();
        $this->assertInstanceOf(Form::class, $form);
        
        return $form;
    }
    
    /**
     * @depends testCreateForm
     */
    public function testAttributesFrom(Form $form)
    {
        $attributes = $form->attributes();
        $this->assertIsArray($attributes);
        $this->assertArrayHasKey('estimate', $attributes);
        $this->assertArrayHasKey('tax', $attributes);
        $this->assertArrayHasKey('instalments', $attributes);
        $this->assertArrayHasKey('userDay', $attributes);
        $this->assertArrayHasKey('userHour', $attributes);               
    }
}
