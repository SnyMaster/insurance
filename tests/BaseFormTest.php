<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use lib\BaseForm;

class BaseFormTest extends TestCase
{
    protected $baseFormClass;
    
    protected function setUp() : void
    {
        $this->baseFormClass = new class extends BaseForm {
            public function attributes() 
            {
                return [
                    'integerAttribute' =>  ['type' => 'integer', 'min' => 1, 'max' => 10, 'required' => TRUE],
                    'rangeAttribute' => ['type' => 'range', 'in' => ['a', 'b', 'c'], 'required' => TRUE],
                    'stringAttribute' => ['type' => 'string'],
                    'numberAttribute' => ['type' => 'number', 'min' => 0.1, 'max' => 9.9],
                ];
            }
        };
    }
    
    public function testFullData()
    {
        $data = [
            'integerAttribute' =>  1,
            'rangeAttribute' => 'b',
            'stringAttribute' => 'test',
            'numberAttribute' => 0.2,            
            ];
        $this->assertNotEmpty($data);
        $this->assertArrayHasKey('integerAttribute', $data);
        $this->assertArrayHasKey('rangeAttribute', $data);
        $this->assertArrayHasKey('stringAttribute', $data);
        $this->assertArrayHasKey('numberAttribute', $data);
        
        return $data;
    }        
    
    public function testInstance()
    {
        $this->assertInstanceOf(BaseForm::class, $this->baseFormClass);
    }
    
    /**
     * @depends testFullData
     */
    public function testLoadFullData(array $data)
    {
        $this->assertTrue($this->baseFormClass->load($data));
    }
    
    public function testLoadEmpyData()
    {
        $data = [];
        $this->assertFalse($this->baseFormClass->load($data));
    }
    
    /**
     * @depends testFullData
     */
    public function testLoadDataValid(array $data)
    {
        $this->baseFormClass->load($data);
        $this->assertTrue($this->baseFormClass->validate(), 'Validating valid data return "false"!');
    }    
    
    /**
     * @depends testFullData
     */
    public function testValidDataNoHasErrors(array $data)
    {
        $this->baseFormClass->load($data);
            $this->assertFalse($this->baseFormClass->hasError(), '"hasError return "true" after validate valid data!');
    }        
    
    /**
    * @depends testFullData
    */
    public function testGetDataAfterLoadFullData(array $data)
    {
        $this->baseFormClass->load($data);
        $this->assertSame($this->baseFormClass->getData(), $data);
    }    
    
    /**
    * @depends testFullData
    */
    public function testGetAttrubiteAfterLoadFullData(array $data)
    {
        $this->baseFormClass->load($data);
        $this->assertEquals(1, $this->baseFormClass->integerAttribute);
        $this->assertEquals('b', $this->baseFormClass->rangeAttribute);
        $this->assertEquals('test', $this->baseFormClass->stringAttribute);
        $this->assertEquals(0.2, $this->baseFormClass->numberAttribute);
    }        
    
    /**
    * @depends testFullData
    */
    public function testIntegerSetFloatValidate(array $data)
    {
        $data['integerAttribute'] = 0.2;
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate(), 'Float value validate as integer value');
        $this->assertArrayHasKey('integerAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "integerAttribute"');        
    }    
    
    /**
    * @depends testFullData
    */
    public function testIntegerSetStringValidate(array $data)
    {
        $data['integerAttribute'] = 'string test';
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate(), 'String value validate as integer value');
        $this->assertArrayHasKey('integerAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "integerAttribute"');        
    }    
    
    /**
    * @depends testFullData
    */
    public function testNumberSetStringValidate(array $data)
    {
        $data['numberAttribute'] = 'string test';
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate(), 'String value validate as number value');
        $this->assertArrayHasKey('numberAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "numberAttribute"');        
    }            
    
    /**
    * @depends testFullData
    */
    public function testSanitazeString(array $data)
    {
        $data['stringAttribute'] = '<script>string</script>';
        $this->baseFormClass->load($data);
        
        $this->assertTrue($this->baseFormClass->validate());
        $this->assertArrayNotHasKey('stringAttribute', $this->baseFormClass->getErrors(), 'Errors include "stringAttribute"');        
        $this->assertEquals('string', $this->baseFormClass->stringAttribute);
    }            
    
    /**
    * @depends testFullData
    */
    public function testMaxValueForNumber(array $data)
    {
        $data['numberAttribute'] = 100.1;
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate());
        $this->assertArrayHasKey('numberAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "numberAttribute"');        
    }            
    
    /**
    * @depends testFullData
    */
    public function testMinValueForNumber(array $data)
    {
        $data['numberAttribute'] = 0.01;
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate());
        $this->assertArrayHasKey('numberAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "numberAttribute"');        
    }            
    
    /**
    * @depends testFullData
    */
    public function testMaxRangeForInteger(array $data)
    {
        $data['integerAttribute'] = 100;
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate());
        $this->assertArrayHasKey('integerAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "integerAttribute"');        
    }                
    
    /**
    * @depends testFullData
    */
    public function testMinRangeForInteger(array $data)
    {
        $data['integerAttribute'] = -1;
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate());
        $this->assertArrayHasKey('integerAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "integerAttribute"');        
    }                
    
    /**
    * @depends testFullData
    */
    public function testRangeRule(array $data)
    {
        $data['rangeAttribute'] = 'z';
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate());
        $this->assertArrayHasKey('rangeAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "rangeAttribute"');        
    }                    
    
    /**
    * @depends testFullData
    */
    public function testRequiredRule(array $data)
    {
        unset($data['integerAttribute']);
        $this->baseFormClass->load($data);
        
        $this->assertFalse($this->baseFormClass->validate());
        $this->assertArrayHasKey('integerAttribute', $this->baseFormClass->getErrors(), 'Errors don`t include "integerAttribute"');        
    }                        
}